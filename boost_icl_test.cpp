#include <iostream>
// I expect static bounded intervals to be more efficient than dynamic intervals.
#define BOOST_ICL_USE_STATIC_BOUNDED_INTERVALS
#include <boost/icl/split_interval_map.hpp>

using namespace std;
using namespace boost::icl;


/* The most simple example of an interval_map is an overlap counter.
   If intervals are added that are associated with the value 1,
   all overlaps of added intervals are counted as a result in the
   associated values.
*/
// TODO: test if total_absorber is more efficient than the default partial_absorber.
typedef interval_map<int, int> OverlapCounterT;

void print_overlaps(const OverlapCounterT& counter)
{
    for(OverlapCounterT::const_iterator it = counter.begin(); it != counter.end(); it++)
    {
        auto itv  = (*it).first;
        int overlaps_count = (*it).second;
        if(overlaps_count == 1)
            cout << "in interval " << itv << " intervals do not overlap" << endl;
        else
            cout << "in interval " << itv << ": "<< overlaps_count << " intervals overlap" << endl;
    }
}

void overlap_counter()
{
    OverlapCounterT overlap_counter;
    right_open_interval<int> inter_val;

    inter_val = {4,8};
    cout << "-- adding   " << inter_val << " -----------------------------------------" << endl;
    // operator+=() or add() can be used in enableDUChainReferenceCounting().
    overlap_counter += make_pair(inter_val, 1);
    print_overlaps(overlap_counter);
    cout << "-----------------------------------------------------------" << endl;

    inter_val = {6,9};
    cout << "-- adding   " << inter_val << " -----------------------------------------" << endl;
    overlap_counter += make_pair(inter_val, 1);
    print_overlaps(overlap_counter);
    cout << "-----------------------------------------------------------" << endl;

    inter_val = {1,9};
    cout << "-- adding   " << inter_val << " -----------------------------------------" << endl;
    overlap_counter += make_pair(inter_val, 1);
    print_overlaps(overlap_counter);
    cout << "-----------------------------------------------------------" << endl;

    cout << boolalpha;

    auto interval = right_open_interval<int>(3, 7);
    // The contains(map, interval) query can be asserted before subtraction: in the case
    // of DUChainReferenceCounting the subtracted interval must always exist in the map.
    cout << "-- subtracting" << interval << ' ' << contains(overlap_counter, interval) << endl;
    // operator-=() or subtract() can be used in disableDUChainReferenceCounting().
    overlap_counter.subtract(pair{interval, 1});
    print_overlaps(overlap_counter);
    cout << "-----------------------------------------------------------" << endl;

    interval = {3, 7};
    cout << "-- subtracting" << interval << ' ' << contains(overlap_counter, interval) << endl;
    overlap_counter.subtract({interval, 1});
    print_overlaps(overlap_counter);
    cout << "-----------------------------------------------------------" << endl;

    for (int i = 0; i < 11; ++i) {
        // The contains(map, value) query can be used in shouldDoDUChainReferenceCounting().
        cout << "Contains " << i << ": " << contains(overlap_counter, i) << endl;
    }
}

int main()
{
    cout << ">>Interval Container Library: Sample overlap_counter.cpp <<\n";
    cout << "-----------------------------------------------------------\n";
    overlap_counter();
    return 0;
}

// Program output:

// >>Interval Container Library: Sample overlap_counter.cpp <<
// -----------------------------------------------------------
// -- adding   [4,8) -----------------------------------------
// in interval [4,8) intervals do not overlap
// -----------------------------------------------------------
// -- adding   [6,9) -----------------------------------------
// in interval [4,6) intervals do not overlap
// in interval [6,8): 2 intervals overlap
// in interval [8,9) intervals do not overlap
// -----------------------------------------------------------
// -- adding   [1,9) -----------------------------------------
// in interval [1,4) intervals do not overlap
// in interval [4,6): 2 intervals overlap
// in interval [6,8): 3 intervals overlap
// in interval [8,9): 2 intervals overlap
// -----------------------------------------------------------
// -- subtracting[3,7) true
// in interval [1,3) intervals do not overlap
// in interval [4,6) intervals do not overlap
// in interval [6,7): 2 intervals overlap
// in interval [7,8): 3 intervals overlap
// in interval [8,9): 2 intervals overlap
// -----------------------------------------------------------
// -- subtracting[3,7) false
// in interval [1,3) intervals do not overlap
// in interval [6,7) intervals do not overlap
// in interval [7,8): 3 intervals overlap
// in interval [8,9): 2 intervals overlap
// -----------------------------------------------------------
// Contains 0: false
// Contains 1: true
// Contains 2: true
// Contains 3: false
// Contains 4: false
// Contains 5: false
// Contains 6: true
// Contains 7: true
// Contains 8: true
// Contains 9: false
// Contains 10: false
